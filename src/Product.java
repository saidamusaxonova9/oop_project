public class Product {
    private String name;
    private int categoryId;

    public Product(String productName, int categoryId) {
        this.name = productName;
        this.categoryId = categoryId;
    }

    public String getName() {
        return name;
    }

    public int getCategoryId() {
        return categoryId;
    }
}
