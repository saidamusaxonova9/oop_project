public interface Action {
    void execute(DatabaseConnection dbConnection);
}
