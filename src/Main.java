import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws ClassNotFoundException {
        Class.forName("com.mysql.cj.jdbc.Driver");
        String dbUrl = "jdbc:mysql://localhost/course_management_system";
        String dbUser = "root";
        String dbPassword = "20030801@@!!@@!";
        DatabaseConnection dbConnection = new DatabaseConnection(dbUrl, dbUser, dbPassword);
        Administrator admin = new Administrator("admin", "admin");

        runApplication(dbConnection, admin);
    }

    private static void runApplication(DatabaseConnection dbConnection, Administrator admin) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Welcome to the Course Management System");
        System.out.print("Enter Username: ");
        String username = scanner.nextLine();
        System.out.print("Enter Password: ");
        String password = scanner.nextLine();

        if (admin.authorize(username, password)) {
            System.out.println("Authorized successfully.");
            showAdminMenu(dbConnection, admin);
        } else {
            System.out.println("Authorization failed.");
        }
    }

    private static void showAdminMenu(DatabaseConnection dbConnection, Administrator admin) {
        Scanner scanner = new Scanner(System.in);
        boolean running = true;

        while (running) {
            System.out.println("\nAdministrator Menu:");
            System.out.println("1. Add Product");
            System.out.println("2. Delete Product");
            System.out.println("3. Add Category");
            System.out.println("4. Exit");

            System.out.print("Choose an option: ");
            int choice = scanner.nextInt();
            scanner.nextLine();

            switch (choice) {
                case 1:
                    System.out.print("Enter product name: ");
                    String productName = scanner.nextLine();
                    System.out.print("Enter category ID: ");
                    int categoryId = scanner.nextInt();
                    Product newProduct = new Product(productName, categoryId);
                    addProduct(dbConnection, newProduct);
                    break;
                case 2:
                    System.out.print("Enter product ID to delete: ");
                    int productId = scanner.nextInt();
                    deleteProduct(dbConnection, productId);
                    break;
                case 3:
                    System.out.print("Enter category name: ");
                    String categoryName = scanner.next();
                    Category newCategory = new Category(categoryName);
                    addCategory(dbConnection, newCategory);
                    break;
                case 4:
                    running = false;
                    break;
                default:
                    System.out.println("Invalid choice. Please try again.");
                    break;
            }
        }
    }

    private static void addProduct(DatabaseConnection dbConnection, Product product) {
        String sql = "INSERT INTO products (name, category_id) VALUES (?, ?)";
        try {
            PreparedStatement pstmt = dbConnection.getConnection().prepareStatement(sql);
            pstmt.setString(1, product.getName());
            pstmt.setInt(2, product.getCategoryId());
            pstmt.executeUpdate();
            System.out.println("Product added successfully.");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private static void deleteProduct(DatabaseConnection dbConnection, int productId) {
        String sql = "DELETE FROM products WHERE id = ?";
        try {
            PreparedStatement pstmt = dbConnection.getConnection().prepareStatement(sql);
            pstmt.setInt(1, productId);
            pstmt.executeUpdate();
            System.out.println("Product deleted successfully.");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private static void addCategory(DatabaseConnection dbConnection, Category category) {
        try {
            PreparedStatement pstmt = dbConnection.getConnection().prepareStatement("INSERT INTO categories (name) VALUES (?)");
            pstmt.setString(1, category.getName());
            pstmt.executeUpdate();
            System.out.println("Category added successfully.");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
