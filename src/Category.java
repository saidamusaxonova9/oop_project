public class Category {
    private int id;
    private String name;

    public Category(String categoryName) {
        this.name = categoryName;
    }

    public String getName() {
        return name;
    }
}
